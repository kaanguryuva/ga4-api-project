<?php

namespace App\Controller;

use App\Service\AnalyticRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AnalyticController extends AbstractController
{

    #[Route('/', name: 'index')]
    public function index(): Response{

        $metrics = array();
        return $this->render('analytic/index.html.twig', [
            'data' => $metrics,
        ]);

    }

    #[Route('/api/get-live-users/', methods: ['GET'], name: 'api_get_live_users')]
    public function getLiveUsers(AnalyticRepository $analyticRepository): Response
    {

        $metrics = array();
        $data = $analyticRepository->getLiveUsers();

       

        foreach ($data->getRows() as $row) {

            //$dimension['title'] = $row->getDimensionValues()[0]->getValue();
            $dimension['value'] = $row->getMetricValues()[0]->getValue();
    
            $metrics[] = $dimension;
          }
        



        return $this->json($metrics);
    }
}
