<?php 

namespace App\Service;

use Google\Analytics\Data\V1beta\BetaAnalyticsDataClient;
use Google\Analytics\Data\V1beta\DateRange;
use Google\Analytics\Data\V1beta\Dimension;
use Google\Analytics\Data\V1beta\Metric;
//use Google\Analytics\Data\V1beta\RunRealtimeReportRequest;

class AnalyticRepository 
{
  
  private $client;
  private $property_id;

  public function __construct() {
    
    $this->property_id = '310285552';
    $this->client = new BetaAnalyticsDataClient();

  }

  public function getLiveUsers(){


    $response = $this->client->runRealTimeReport([
      'property' => 'properties/' . $this->property_id,
      
      'metrics' => [new Metric(
          [
              'name' => 'activeUsers',
          ]
      )
      ]
     ]);
    

    // $response = $this->client->runReport([
    //   'property' => 'properties/' . $this->property_id,
    //   'dateRanges' => [
    //       new DateRange([
    //           'start_date' => '2023-08-01',
    //           'end_date' => 'today',
    //       ]),
    //   ],
    //   'dimensions' => [new Dimension(
    //       [
    //           'name' => 'city',
    //       ]
    //   ),
    //   ],
    //   'metrics' => [new Metric(
    //       [
    //           'name' => 'activeUsers',
    //       ]
    //   )
    //   ]
    //  ]);

     return $response;



  }
  
}
