import { Controller } from '@hotwired/stimulus';
import axios from 'axios';

/*
 * This is an example Stimulus controller!
 *
 * Any element with a data-controller="hello" attribute will cause
 * this controller to be executed. The name "hello" comes from the filename:
 * hello_controller.js -> "hello"
 *
 * Delete this file or adapt it for your use!
 */
export default class extends Controller {

  static values = {refreshInterval: Number, apiUrl: String }

  connect(){
    this.getData()

    console.log(this.apiUrlValue);

    // if (this.hasRefreshIntervalValue) {
    //   this.startRefreshing()
    // }
  }

  startRefreshing() {
    // setInterval(() => {
    //   this.load()
    // }, this.refreshIntervalValue)
  }

  getData(){
    //console.log(this.element)
    let element = this.element;

    axios.get(this.apiUrlValue)
    .then(function(response){
      element.innerHTML = response.data[0].value;
      //this.element.innerHTML = response.data[0].value;
    });

  }
}
